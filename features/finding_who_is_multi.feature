Feature: Finding who is multi

  Scenario: Finding who is link
     Given I am on the multi website
     When I rollover Qui sommes nous
     Then I see a link "La coopérative"

  Scenario: Finding the team
     Given I am on the multi website
     When I rollover Qui sommes nous
     And I click on Equipe
     Then I see 7 people
     And I must find "Pierre"
