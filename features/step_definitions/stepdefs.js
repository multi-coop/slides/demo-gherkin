import { Given, When, Then, AfterAll } from '@cucumber/cucumber'
import { Builder, By, Capabilities, until } from 'selenium-webdriver'
import { expect } from 'chai';

import "chromedriver";

const capabilities = Capabilities.chrome();
capabilities.set('chromeOptions', { "w3c": false });
const driver = new Builder().withCapabilities(capabilities).build();
driver.manage().window().maximize()

Given('I am on the multi website', async function () {
  await driver.get('https://www.multi.coop');
  await driver.wait(
    until.elementLocated(By.id('multi-site-app-top')),
    10000
  )
});

When('I rollover Qui sommes nous', async function () {
  const whoLink = await driver.findElement(By.css('.navbar-start :first-child'))
  await driver.actions().move({origin: whoLink}).perform()
})

When('I click on Equipe', async function () {
  const teamLink = await driver.findElement(
    By.css('.navbar-item.has-dropdown .navbar-dropdown :nth-child(3) a')
  )

  await driver.wait(
    until.elementIsVisible(teamLink),
    10000
  )

  await driver.actions().click(teamLink).perform()
})

Then('I see a link {string}', async function (pattern) {
  const dropdown = await driver.findElement(By.css('.navbar-start :first-child .navbar-dropdown'))
  await driver.wait(
    until.elementIsVisible(dropdown),
    10000
  )
  expect(await dropdown.getText()).to.contains(pattern)
});

Then('I see {int} people', async function (teamSize) {
  await driver.wait(
    until.elementsLocated(By.css('.dataCard')),
    10000
  )

  const teamCards = await driver.findElements(By.css('.dataCard'))
  expect(teamCards.length).to.equal(teamSize)
});

Then('I must find {string}', async function (person) {
  await driver.wait(
    until.elementTextContains(driver.findElement(By.css('body')), person),
    20000
  )

  const cards = await driver.findElements(By.css('.dataCard h2'))
  cards.map(async card => console.log('-', await card.getText()))
});

AfterAll(async function(){
  await driver.quit();
});
